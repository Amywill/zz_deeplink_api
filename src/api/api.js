import request from '@/utils/request'
let obj = {
	// 用户信息上报
	postUsrInfo(params) {
		// real:
		// https://api-zepeto.kajicam.com/zepeto/activity/user/center/api/user-info/report
		// beta:
		// https://api-zepeto-beta.kajicam.com/zepeto/activity/user/center/api/user-info/report
		
		return request({
			url: `https://api-zepeto-beta.kajicam.com/zepeto/activity/user/center/api/user-info/report`,
			// url: `https://api-zepeto.kajicam.com/zepeto/activity/user/center/api/user-info/report`,
			headers:{
				"Content-Type": "application/json",
			},
			data: params,
			method: 'POST',
			timeout: 10000
		})
  },
  
	// 关注
	setFollow(params) {
    console.log(params)
		return request({
			// TODO 改成real
			// url: `https://api-zepeto.kajicam.com/zepeto/activity/fashion/pk/api/fashionPk/follow/add_follow?token=${encodeURIComponent(params.token)}&followUserId=${params.userId}&duid=${params.duid}&ua=${params.ua}`,
			url: `https://api-zepeto-beta.kajicam.com/zepeto/activity/creative/api/ranking/follow/add_follow?token=${encodeURIComponent(params.token)}&followUserId=${params.userId}&duid=${params.duid}&ua=${params.ua}`,
			headers:{
				"Content-Type": "application/json",
			},
			method: 'POST',
			timeout: 50000
		})
  },

  // 渲染崽崽动作形象 单图
	renderSingleImage({poseId, hashcodes}) {
		let params = {
			"type": "booth",
			"target": {
				"hashCodes": [
					`${hashcodes}`
				]
			}
		}
		return request({
			url: `https://render-api.zepeto.cn/v2/graphics/zaizai/booth/${poseId}?cdn=true&cache=true&binary=false&permanent=true`,
			headers:{
				"Content-Type": "application/json",
			},
			data: params,
			method: 'POST',
			timeout: 50000
		})
  },

  // 渲染崽崽动作形象 多图

  // cdn=true&cache=true&permanent=true 这几个键值对不传时都默认为true，不要设置为false，除非有特殊需求，见韩国技术文档Swagger地址：`https://render-api.zepeto.cn/v2/swagger/#/`

  renderMultipleImage({boothIds, hashCodes}) {
		// let params = {
    //   "type": "booth", // 默认值，必填
    //   "ids": [
    //     "PHOTOBOOTH_TWO_280",
    //     "PHOTOBOOTH_TWO_85"
    //   ],
    //   "target": {
    //     "hashCodes": [
    //       "VLUFUV",
    //       "ZVGDAK"
    //     ]
    //   }
    // }
    let params = {
      "type": "booth", // 默认值，必填
      "ids": boothIds,
      "target": {
        "hashCodes": hashCodes
      }
    }
    console.log(params)
		return request({
      url: `https://render-api.zepeto.cn/v2/graphics/zaizai/booth?cdn=true&cache=true&permanent=true`,
			headers:{
				"Content-Type": "application/json",
			},
			data: params,
			method: 'POST',
			timeout: 50000
		})
  },


}
export default obj