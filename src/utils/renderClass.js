export default new class renderClass {

  constructor() {
    this.userInfo = {}
  }

  static getInstance() {
    return null;
  }

  /**
   * 获取用户信息
   */
  UserInfo() {
    let timer1 = setInterval(() => {
      if (window.ZEPETO) {
        this.userInfo = window.ZEPETO
        console.log(window.ZEPETO)
        clearInterval(timer1) 
        return window.ZEPETO
      }
    }, 20)
    let timer2 = setTimeout(() => {
      clearInterval(timer1)
      clearTimeout(timer2)
    }, 3000)
  }

  /**
   * 关闭H5
   */
  Close() {
    window.location.href = 'ZEPETO://WEBVIEW/CLOSE'
  }

  /**
   * 存储离开前H5的url
   */
  StoreUrl() {
    let currentUrl = window.location.href
    let currUrl
    let referrerUri
    if (currentUrl.indexOf('#') !== -1) {
      currUrl = currentUrl.split('#')[0]
    } else {
      currUrl = currentUrl
    }
    referrerUri = encodeURIComponent(`zepeto://home/webview?url=${currUrl}`)
    return referrerUri
  }

  /**
   * 保存base64格式的图到相册
   * @param {Function} callback // callback可以是保存成功的提示
   * @param {String} base64 // 去掉开头"data:image/png;base64,"
   */
  SaveImage(callback, base64) {
    let newBase64 = base64.substring(base64.indexOf(',')+1); 
    if (window.ZEPETO) {
      window.ZEPETO.invoke('SaveImage', newBase64, callback); 
    }
  }

  

  /**
   * 上传到feed
   * @param {String} base64 
   */
  Feed(base64) {
    const toFeed = () => {
      window.location.href = 'ZEPETO://HOME/FEED/UPLOAD/CAMERAROLL'
    }
    this.SaveImage(toFeed, base64)
  }

  /**
   * 试穿
   */
  HadItem() {
    // window.location.href = `ZEPETO://HOME/SHOP/OOTD/CONTENTS/PREVIEW?hashCode=ZVGDAK&contentIds=PRESET_MOUTH_25,PRESET_MOUTH_27,PRESET_MOUTH_26,NECKLACE_2,EARRING_33,HEADWEAR_18`
    window.location.href = `ZEPETO://HOME/SHOP/OOTD/CONTENTS/PREVIEW?contentIds=PRESET_MOUTH_25,PRESET_MOUTH_27,PRESET_MOUTH_26,NECKLACE_2,EARRING_33,HEADWEAR_18`
  }

  /**
   * 打开他人个人主页/打开他人个人中心
   */
  VisitOtherCenter(hashCode) {
    console.log('Amy',hashCode)
    console.log(this.userInfo)
    window.location.href = `ZEPETO://HOME/PROFILE/CARD?${hashCode}`
  }

  /**
   * 打开个人主页MENU
   */
  OpenHomeMenu() {
    window.location.href = `ZEPETO://HOME/MENU`
  }

  /**
   * 打开个人主页名称
   */
  OpenHomeName() {
    window.location.href = `ZEPETO://HOME/NAME`
  }

  /**
   * 打开个人中心
   */
  OpenUserCenter() {
    window.location.href = `ZEPETO://HOME`
  }

  /**
   * 拜访
   * @param {String} hashCode 
   */
  Visit(hashCode) {
    window.location.href = `ZEPETO://HOME/FRIEND/VISIT?${hashCode}`
  }

  /**
   * 打开崽崽世界
   */
  OpenWorld() {
    window.location.href = `ZEPETO://WORLDLOAD`
  }

  /**
   * 打开通知页，可返回H5
   */
  OpenNotify() {
    window.location.href = `ZEPETO://HOME/NOTIFICATIONS?referrer=${this.StoreUrl()}`
  }

  /**
   * 打开礼物盒，可返回H5
   */
  OpenGiftBox() {
    window.location.href = `ZEPETO://HOME/NOTIFICATIONS/GIFTBOX?referrer=${this.StoreUrl()}`
  }

  /**
   * 去换衣服/试衣间，实际是去商店（可分类），可返回H5
   */
  OpenStore() {
    window.location.href = `ZEPETO://HOME/SHOP/COSTUME?referrer=${this.StoreUrl()}`
  }

  /**
   * 去商店（姿势分类），可返回H5
   */
  OpenStorePosture() {
    window.location.href = `ZEPETO://HOME/SHOP/ANIMATION?referrer=${this.StoreUrl()}`
  }

  /**
   * 去商店（发型分类），可返回H5
   */
  OpenStoreHairs() {
    window.location.href = `ZEPETO://HOME/SHOP/HAIR?referrer=${this.StoreUrl()}`
  }

  /**
  * 去商店（裤子分类），可返回H5
  */
  OpenStorePants() {
    window.location.href = `ZEPETO://HOME/SHOP/BOTTOM?referrer=${this.StoreUrl()}`
  }

  /**
   * 去商店（袜子分类），可返回H5
   */
  OpenStoreSocks() {
    window.location.href = `ZEPETO://HOME/SHOP/SOCKS?referrer=${this.StoreUrl()}`
  }

  /**
   * 去商店（家具分类），可返回H5
   */
  OpenStoreRoom() {
    window.location.href = `ZEPETO://HOME/SHOP/ROOM?referrer=${this.StoreUrl()}`
  }

  /**
   * 打开房屋装饰，可返回H5
   */
  OpenRoomDecoration() {
    window.location.href = `ZEPETO://HOME/ROOM/EDIT?referrer=${this.StoreUrl()}`
  }

  /**
   * 打开地板装饰，可返回H5
   */
  OpenFloorDecoration() {
    window.location.href = `ZEPETO://HOME/ROOM/EDIT/FLOOR?referrer=${this.StoreUrl()}`
  }



  /**
   * 去拍照分栏
   */
  OpenBooth() {
    window.location.href = `ZEPETO://HOME/MENU/BOOTH?referrer=${this.StoreUrl()}`
  }

  /**
   * 打开拍摄，使用自己的崽崽头像，这三个打开的效果是一样的，可能韩国没开发完
   */
  OpenCameraCapture() {
    window.location.href = `ZEPETO://HOME/CAPTURE?referrer=${this.StoreUrl()}`
  }
  OpenCameraPhoto() {
    window.location.href = `ZEPETO://HOME/CAPTURE/PHOTO?referrer=${this.StoreUrl()}`
  }
  OpenCameraVideo() {
    window.location.href = `ZEPETO://HOME/CAPTURE/VIDEO?referrer=${this.StoreUrl()}`
  }


  /**
   * 跳转到关注的好友聊天列表，不能返回H5
   */
  OpenFriendList1() {
    window.location.href = `ZEPETO://HOME/CHAT?referrer=${this.StoreUrl()}`
  }

  /**
   * 跳转到和某个用户的聊天界面，返回到聊天列表（25.1），再返回到H5
   * @param {String} userId 
   */
  OpenFriendList2(userId) {
    window.location.href = `zepeto-cn://home/chat/dm/${userId}?referrer=${this.StoreUrl()}`
  }

  /**
   * 跳转到和某个用户的聊天界面，直接返回到H5
   * @param {String} userId 
   */
  OpenFriendList3(userId) {
    window.location.href = `zepeto-cn://home/chatroom/dm/${userId}?referrer=${this.StoreUrl()}`;
  }

  /**
   * 商店展示指定物件
   */
  GoodsList() {
    window.location.href = `ZEPETO://HOME/SHOP/CONTENTS?TOP_100,TOP_200,BTM_40`
  }
}