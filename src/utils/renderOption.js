
export default class renderOption {
  constructor({type="booth", hashCodes="[ZVGDAK,1LTI1E]", userId="5e0454e489dbbe3e8ff04150"}) {
    this.type = type;
    this.hashCodes = hashCodes;
    this.userId = userId;
  }
}