/**
 * author: Amy 
 * date: 2020.05.08
 */

const zzObj = {

  // 存储离开H5时的url
  storeCurrUrl() {
    let currentUrl = window.location.href
    let currUrl
    let referrerUri
    if (currentUrl.indexOf('#') !== -1) {
      currUrl = currentUrl.split('#')[0]
    } else {
      currUrl = currentUrl
    }
    referrerUri = encodeURIComponent(`zepeto://home/webview?url=${currUrl}`)
    return referrerUri
  },

  // 2. 获取崽崽用户信息
  getUserInfo() {
    let timer1 = setInterval(() => {
      if (window.ZEPETO) {
        alert('请在控制台中查看window.ZEPETO')
        console.log(window.ZEPETO)
        clearInterval(timer1) 
      }
    }, 20)
    let timer2 = setTimeout(() => {
      clearInterval(timer1)
      clearTimeout(timer2)
    }, 3000)
  },

  // 4. 关闭
  closeWebview() {
    window.location.href = 'ZEPETO://WEBVIEW/CLOSE'
  },

  // 5. 打开个人主页/去个人中心
  goProfile(hashCode) {
    window.location.href = `ZEPETO://HOME/PROFILE/CARD?${hashCode}`
  },

  // 6. 去换衣服/试衣间，实际是去商店（可分类），可返回H5
  goStore() {
    window.location.href = `ZEPETO://HOME/SHOP/COSTUME?referrer=${this.storeCurrUrl()}`
  },

  // 6.1 去商店（姿势分类），可返回H5
  goStorePosture() {
    window.location.href = `ZEPETO://HOME/SHOP/ANIMATION?referrer=${this.storeCurrUrl()}`
  },

  // 6.2 去商店（发型分类），可返回H5
  goStoreHairs() {
    window.location.href = `ZEPETO://HOME/SHOP/HAIR?referrer=${this.storeCurrUrl()}`
  },

  // 6.3 去商店（裤子分类），可返回H5
  goStorePants() {
    window.location.href = `ZEPETO://HOME/SHOP/BOTTOM?referrer=${this.storeCurrUrl()}`
  },

  // 6.4 去商店（袜子分类），可返回H5
  goStoreSocks() {
    window.location.href = `ZEPETO://HOME/SHOP/SOCKS?referrer=${this.storeCurrUrl()}`
  },

  // 6.5 去商店（家具分类），可返回H5
  goStoreRoom() {
    window.location.href = `ZEPETO://HOME/SHOP/ROOM?referrer=${this.storeCurrUrl()}`
  },

  // 7. 保存图片到相册
  handleSave(base64) {
    const saveSucess = () => {
      alert('保存成功')
    }
    this.saveImage(saveSucess, base64)
  },

  // 8. 将图片上传到Feed
  handleUpload(base64) {
    const toFeed = () => {
      window.location.href = 'ZEPETO://HOME/FEED/UPLOAD/CAMERAROLL'
    }
    this.saveImage(toFeed, base64)
  },

  // 13. 部件（衣服，裤子等）穿戴
  hadItem() {
    window.location.href = `ZEPETO://HOME/SHOP/OOTD/CONTENTS/PREVIEW?hashCode=L8NKZB&contentIds=DR_202,F_HAIR_338`
  },

  // 14. 去拍照分栏
  goPhoto() {
    window.location.href = `ZEPETO://HOME/MENU/BOOTH?referrer=${this.storeCurrUrl()}`
  },

  // 15.1 打开拍摄，使用自己的崽崽头像，这三个打开的效果是一样的，可能韩国没开发完
  openCamera1() {
    window.location.href = `ZEPETO://HOME/CAPTURE?referrer=${this.storeCurrUrl()}`
  },
  // 15.2
  openCamera2() {
    window.location.href = `ZEPETO://HOME/CAPTURE/PHOTO?referrer=${this.storeCurrUrl()}`
  },
  // 15.3
  openCamera3() {
    window.location.href = `ZEPETO://HOME/CAPTURE/VIDEO?referrer=${this.storeCurrUrl()}`
  },

  // 16.1 打开主页Menu
  openMenu() {
    window.location.href = `ZEPETO://HOME/MENU`
  },

  // 16.2 打开主页和名称
  openMainPage() {
    window.location.href = `ZEPETO://HOME/NAME`
  },

  // 17.1 打开房屋装饰，可返回H5
  openRoomDecoration() {
    window.location.href = `ZEPETO://HOME/ROOM/EDIT?referrer=${this.storeCurrUrl()}`
  },

  // 17.2 打开地板装饰，可返回H5
  openFloorDecoration() {
    window.location.href = `ZEPETO://HOME/ROOM/EDIT/FLOOR?referrer=${this.storeCurrUrl()}`
  },

  // 18. 打开崽崽世界
  openWorld() {
    window.location.href = `ZEPETO://WORLDLOAD`
  },

  // 19. 打开通知页，可返回H5
  openNotify() {
    window.location.href = `ZEPETO://HOME/NOTIFICATIONS?referrer=${this.storeCurrUrl()}`
  },

  // 20. 打开礼物盒，可返回H5
  openGiftBox() {
    window.location.href = `ZEPETO://HOME/NOTIFICATIONS/GIFTBOX?referrer=${this.storeCurrUrl()}`
  },

  // 21. 返回主页
  goMainPage() {
    window.location.href = `ZEPETO://HOME`
  },

  // 22. 拜访
  goVisit(hashCode) {
    window.location.href = `ZEPETO://HOME/FRIEND/VISIT?${hashCode}`
  },

  // 23. 商店展示指定物件
  showList() {
    window.location.href = `ZEPETO://HOME/SHOP/CONTENTS?TOP_100,TOP_200,BTM_40`
  },

  // 25.1 跳转到关注的好友聊天列表，不能返回H5
  goFriendList1() {
    window.location.href = `ZEPETO://HOME/CHAT?referrer=${this.storeCurrUrl()}`
  },

  // 25.2 跳转到和某个用户的聊天界面，返回到聊天列表（25.1），再返回到H5
  goFriendList2(userId) {
    window.location.href = `zepeto-cn://home/chat/dm/${userId}?referrer=${this.storeCurrUrl()}`
  },

  // 25.3 跳转到和某个用户的聊天界面，直接返回到H5
  goFriendList3(userId) {
    window.location.href = `zepeto-cn://home/chatroom/dm/${userId}?referrer=${this.storeCurrUrl()}`;
  }
}

export default zzObj