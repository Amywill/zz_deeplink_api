import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { Swipe, SwipeItem, Tab, Tabs, List, Cell, Lazyload, Col, Row, Collapse, CollapseItem } from 'vant';

import './assets/css/reset.css'
import './assets/css/common.scss'

Vue.config.productionTip = false

if (process.env.NODE_ENV !== 'production') {
  showDebugger()
}

function showDebugger () {
  const VConsole = require('vconsole/dist/vconsole.min.js')
  new VConsole()

  const button = document.createElement('button')
  button.onclick = () => {
    window.location.reload()
  }
  button.style.cssText = `
    display:block;
    font-size: 14px;
    position:fixed;
    left:0.769em;
    bottom:0.769em;
    color:#fff;
    background-color:#04BE02;
    height: 2.4em;
    line-height:1;
    font-size:14px;
    padding:0.61538462em 1.23076923em;
    z-index:10000;
    border-radius:0.30769231em;
    box-shadow:0 0 0.61538462em rgba(0,0,0,0.4);
  `
  button.innerText = 'reload-200504'
  button.addEventListener('touchmove', function (e) {
    e.preventDefault();
    const moveEndX = e.changedTouches[0].pageX;
    const moveEndY = e.changedTouches[0].pageY;

    const $this = e.target
    const xMM = moveEndX - $this.clientWidth / 2;
    const yMM = moveEndY - $this.clientHeight / 2;
    if (xMM < document.documentElement.clientWidth - $this.clientWidth && xMM > 0) {
      $this.style.left = xMM + 'px'
    }

    if (yMM < document.documentElement.clientHeight - $this.clientHeight && yMM > 0) {
      $this.style.top = yMM + 'px'
    }
  })
  document.body.append(button)
}

Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(List);
Vue.use(Cell);
Vue.use(Lazyload);
Vue.use(Col);
Vue.use(Row);
Vue.use(Collapse); 
Vue.use(CollapseItem);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
